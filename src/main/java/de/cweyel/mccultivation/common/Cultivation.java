package de.cweyel.mccultivation.common;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.cweyel.mccultivation.common.registration.Registration;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(Cultivation.MOD_ID)
public class Cultivation {
    public static final String MOD_ID = "cultivation";

    public static final Logger LOGGER = LogManager.getLogger();

    public Cultivation() {
        Registration.register();

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }
}
