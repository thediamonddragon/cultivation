package de.cweyel.mccultivation.common.registration;

import java.util.function.Supplier;

import de.cweyel.mccultivation.common.Cultivation;
import de.cweyel.mccultivation.common.block.metalpress.MetalPressBlock;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.fml.RegistryObject;

public class ModBlocks {

    public static final RegistryObject<Block> DIVINE_IRON_ORE = register("divine_iron_ore", () -> new Block(
            AbstractBlock.Properties.of(Material.STONE).harvestLevel(3).strength(15, 5).sound(SoundType.STONE)));

    public static final RegistryObject<Block> DIVINE_IRON_BLOCK = register("divine_iron_block", () -> new Block(
            AbstractBlock.Properties.of(Material.METAL).harvestLevel(3).strength(15, 7).sound(SoundType.METAL)));

    public static final RegistryObject<MetalPressBlock> METAL_PRESS = register("metal_press", () -> new MetalPressBlock(
            AbstractBlock.Properties.of(Material.METAL).strength(4, 20).sound(SoundType.METAL)));

    public static void register() {
        Cultivation.LOGGER.info("registering blocks");
    }

    private static <T extends Block> RegistryObject<T> registerNoItem(String name, Supplier<T> block) {
        return Registration.BLOCKS.register(name, block);
    }

    private static <T extends Block> RegistryObject<T> register(String name, Supplier<T> block) {
        RegistryObject<T> ret = registerNoItem(name, block);
        Registration.ITEMS.register(name,
                () -> new BlockItem(ret.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
        return ret;
    }
}
