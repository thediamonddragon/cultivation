package de.cweyel.mccultivation.common.registration;

import de.cweyel.mccultivation.common.Cultivation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.fml.RegistryObject;

public class ModItems {
    public static RegistryObject<Item> DIVINE_IRON_INGOT = Registration.ITEMS.register("divine_iron_ingot",
            () -> new Item(new Item.Properties().tab(ItemGroup.TAB_MATERIALS)));

    public static void register() {
        Cultivation.LOGGER.info("registering items");
    }
}
