package de.cweyel.mccultivation.generators.tags;

import de.cweyel.mccultivation.common.Cultivation;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;

public class ModTags {
    public static final class Blocks {
        public static final ITag.INamedTag<Block> ORES_DIVINE_IRON = mod("ores/divine_iron");
        public static final ITag.INamedTag<Block> STORAGE_BLOCKS_DIVINE_IRON = mod("sorage_blocks/divine_iron");

        private static ITag.INamedTag<Block> forge(String path) {
            return BlockTags.bind(new ResourceLocation("forge", path).toString());
        }

        private static ITag.INamedTag<Block> mod(String path) {
            return BlockTags.bind(new ResourceLocation(Cultivation.MOD_ID, path).toString());
        }
    }

    public static final class Items {
        public static final ITag.INamedTag<Item> ORES_DIVINE_IRON = mod("ores/divine_iron");
        public static final ITag.INamedTag<Item> STORAGE_BLOCKS_DIVINE_IRON = mod("sorage_blocks/divine_iron");

        public static final ITag.INamedTag<Item> INGOTS_DIVINE_IRON = mod("ingots/divine_iron");

        private static ITag.INamedTag<Item> forge(String path) {
            return ItemTags.bind(new ResourceLocation("forge", path).toString());
        }

        private static ITag.INamedTag<Item> mod(String path) {
            return ItemTags.bind(new ResourceLocation(Cultivation.MOD_ID, path).toString());
        }
    }
}
