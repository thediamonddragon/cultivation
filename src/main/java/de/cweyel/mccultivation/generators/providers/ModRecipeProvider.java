package de.cweyel.mccultivation.generators.providers;

import java.util.function.Consumer;

import de.cweyel.mccultivation.common.Cultivation;
import de.cweyel.mccultivation.common.registration.ModBlocks;
import de.cweyel.mccultivation.common.registration.ModItems;
import net.minecraft.data.CookingRecipeBuilder;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.data.RecipeProvider;
import net.minecraft.data.ShapedRecipeBuilder;
import net.minecraft.data.ShapelessRecipeBuilder;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;

public class ModRecipeProvider extends RecipeProvider {

    public ModRecipeProvider(DataGenerator generator) {
        super(generator);
    }

    @Override
    protected void buildShapelessRecipes(Consumer<IFinishedRecipe> consumer) {
        ShapedRecipeBuilder.shaped(ModBlocks.DIVINE_IRON_BLOCK.get(), 1).define('#', ModItems.DIVINE_IRON_INGOT.get())
                .pattern("###").pattern("###").pattern("###")
                .unlockedBy("has_item", has(ModItems.DIVINE_IRON_INGOT.get())).save(consumer);
        ShapelessRecipeBuilder.shapeless(ModItems.DIVINE_IRON_INGOT.get(), 9)
                .requires(ModBlocks.DIVINE_IRON_BLOCK.get())
                .unlockedBy("has_item", has(ModItems.DIVINE_IRON_INGOT.get()))
                .save(consumer, modid("divine_iron_ingot_from_block"));
        CookingRecipeBuilder
                .smelting(Ingredient.of(ModBlocks.DIVINE_IRON_ORE.get()), ModItems.DIVINE_IRON_INGOT.get(), 0.7f, 300)
                .unlockedBy("has_item", has(ModBlocks.DIVINE_IRON_ORE.get()))
                .save(consumer, modid("divine_iron_ingot_from_ore_smelting"));
        CookingRecipeBuilder
                .blasting(Ingredient.of(ModBlocks.DIVINE_IRON_ORE.get()), ModItems.DIVINE_IRON_INGOT.get(), 0.7f, 150)
                .unlockedBy("has_item", has(ModBlocks.DIVINE_IRON_ORE.get()))
                .save(consumer, modid("divine_iron_ingot_from_ore_blasting"));
    }

    private ResourceLocation modid(String path) {
        return new ResourceLocation(Cultivation.MOD_ID, path);
    }
}
