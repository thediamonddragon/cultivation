package de.cweyel.mccultivation.generators.providers;

import de.cweyel.mccultivation.common.Cultivation;
import de.cweyel.mccultivation.common.registration.ModBlocks;
import de.cweyel.mccultivation.generators.tags.ModTags;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ExistingFileHelper;

public class ModBlockTagsProvider extends BlockTagsProvider {

    public ModBlockTagsProvider(DataGenerator p_i48256_1_, ExistingFileHelper existingFileHelper) {
        super(p_i48256_1_, Cultivation.MOD_ID, existingFileHelper);
    }

    @Override
    protected void addTags() {
        tag(ModTags.Blocks.ORES_DIVINE_IRON).add(ModBlocks.DIVINE_IRON_ORE.get());
        tag(Tags.Blocks.ORES).addTag(ModTags.Blocks.ORES_DIVINE_IRON);
        tag(ModTags.Blocks.STORAGE_BLOCKS_DIVINE_IRON).add(ModBlocks.DIVINE_IRON_BLOCK.get());
        tag(Tags.Blocks.STORAGE_BLOCKS).addTag(ModTags.Blocks.STORAGE_BLOCKS_DIVINE_IRON);
    }

}
