package de.cweyel.mccultivation.generators.providers;

import de.cweyel.mccultivation.common.Cultivation;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;

public class ModItemModelProvider extends ItemModelProvider {

    public ModItemModelProvider(DataGenerator generator, ExistingFileHelper existingFileHelper) {
        super(generator, Cultivation.MOD_ID, existingFileHelper);
    }

    @Override
    protected void registerModels() {
        withExistingParent("divine_iron_ore", modLoc("block/divine_iron_ore"));
        withExistingParent("divine_iron_block", modLoc("block/divine_iron_block"));

        ModelFile itemGenerated = getExistingFile(mcLoc("item/generated"));

        builder(itemGenerated, "divine_iron_ingot");
    }

    private ItemModelBuilder builder(ModelFile itemGenerated, String name) {
        return getBuilder(name).parent(itemGenerated).texture("layer0", "item/" + name);
    }

}
