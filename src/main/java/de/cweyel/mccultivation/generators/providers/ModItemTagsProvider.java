package de.cweyel.mccultivation.generators.providers;

import net.minecraft.data.ItemTagsProvider;
import de.cweyel.mccultivation.common.Cultivation;
import de.cweyel.mccultivation.common.registration.ModItems;
import de.cweyel.mccultivation.generators.tags.ModTags;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ExistingFileHelper;

public class ModItemTagsProvider extends ItemTagsProvider {

    public ModItemTagsProvider(DataGenerator p_i232552_1_, BlockTagsProvider blockTagsProvider,
            ExistingFileHelper existingFileHelper) {
        super(p_i232552_1_, blockTagsProvider, Cultivation.MOD_ID, existingFileHelper);
    }

    @Override
    protected void addTags() {
        copy(ModTags.Blocks.ORES_DIVINE_IRON, ModTags.Items.ORES_DIVINE_IRON);
        copy(ModTags.Blocks.STORAGE_BLOCKS_DIVINE_IRON, ModTags.Items.STORAGE_BLOCKS_DIVINE_IRON);

        tag(ModTags.Items.INGOTS_DIVINE_IRON).add(ModItems.DIVINE_IRON_INGOT.get());
        tag(Tags.Items.INGOTS).addTag(ModTags.Items.INGOTS_DIVINE_IRON);
    }
}
