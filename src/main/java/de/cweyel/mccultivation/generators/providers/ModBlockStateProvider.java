package de.cweyel.mccultivation.generators.providers;

import de.cweyel.mccultivation.common.Cultivation;
import de.cweyel.mccultivation.common.registration.ModBlocks;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

public class ModBlockStateProvider extends BlockStateProvider {

    public ModBlockStateProvider(DataGenerator gen, ExistingFileHelper exFileHelper) {
        super(gen, Cultivation.MOD_ID, exFileHelper);
    }

    @Override
    protected void registerStatesAndModels() {
        simpleBlock(ModBlocks.DIVINE_IRON_ORE.get());
        simpleBlock(ModBlocks.DIVINE_IRON_BLOCK.get());
    }
}
