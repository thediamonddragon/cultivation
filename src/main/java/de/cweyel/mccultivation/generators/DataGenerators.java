package de.cweyel.mccultivation.generators;

import de.cweyel.mccultivation.common.Cultivation;
import de.cweyel.mccultivation.generators.providers.ModBlockStateProvider;
import de.cweyel.mccultivation.generators.providers.ModBlockTagsProvider;
import de.cweyel.mccultivation.generators.providers.ModItemModelProvider;
import de.cweyel.mccultivation.generators.providers.ModItemTagsProvider;
import de.cweyel.mccultivation.generators.providers.ModLootTableProvider;
import de.cweyel.mccultivation.generators.providers.ModRecipeProvider;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

@EventBusSubscriber(modid = Cultivation.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGenerators {

    @SubscribeEvent
    public static void gatherData(GatherDataEvent event) {
        DataGenerator gen = event.getGenerator();
        ExistingFileHelper existingFileHelper = event.getExistingFileHelper();

        gen.addProvider(new ModBlockStateProvider(gen, existingFileHelper));
        gen.addProvider(new ModItemModelProvider(gen, existingFileHelper));

        ModBlockTagsProvider blockTags = new ModBlockTagsProvider(gen, existingFileHelper);
        gen.addProvider(blockTags);
        gen.addProvider(new ModItemTagsProvider(gen, blockTags, existingFileHelper));

        gen.addProvider(new ModRecipeProvider(gen));
        gen.addProvider(new ModLootTableProvider(gen));
    }
}
